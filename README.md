# README #

This GUI program helps create new keystore via Java Keytool utility.

### What is this repository for? ###

Target Audience: PhoneGap, PhoneGap Build or Android developers

### How do I get set up? ###

* Go to download section
* Grab binary base on your machine OS (GUICreateNewKeystore_<OS>.tar|zip|dmg)
* Extract content then run

### Contribution guidelines ###

* Why Pascal / Object Pascal? Cross platform, small footprint, no external dependencies.
* What compiler / IDE? Compiler FreePascal 2.6* and above. IDE 1.2* and above.

### Who do I talk to? ###

* Repo owner or admin via issue tracker