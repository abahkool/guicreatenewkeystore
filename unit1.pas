unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, EditBtn,
  StdCtrls, Buttons, Unit2, LCLIntf, ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    imgStatus: TImage;
    imgStatus1: TImage;
    imgStatus2: TImage;
    lbAskKeytool: TLabel;
    lbAskKeystore: TLabel;
    lbKeystore: TLabel;
    lbKeytool: TLabel;
    odKeytool: TOpenDialog;
    sdTargetKeystore: TSaveDialog;
    spSave: TSpeedButton;
    spNext: TSpeedButton;
    spFind: TSpeedButton;
    procedure fnKeytoolChange(Sender: TObject);
    procedure lbKeystoreClick(Sender: TObject);
    Procedure lbKeytoolClick(Sender: TObject);
    Procedure spFindClick(Sender: TObject);
    procedure spSaveClick(Sender: TObject);
    procedure spNextClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.lbKeystoreClick(Sender: TObject);
begin
  LCLIntf.OpenDocument(ExtractFilePath(unit2.sKeystore));
end;

Procedure TForm1.lbKeytoolClick(Sender: TObject);
Begin
  LCLIntf.OpenDocument(ExtractFilePath(unit2.sKeytool));
end;

Procedure TForm1.spFindClick(Sender: TObject);
Begin
  odKeytool.InitialDir:=GetAppConfigDir(true);
  odKeytool.Title:='Where to find keytool program?';
  if odKeytool.Execute then
     begin
       lbKeytool.Caption:='[' + ExtractFileName(odKeytool.FileName) + ']';
       lbkeytool.Hint:=odKeytool.FileName;
       unit2.sKeytool:=odKeytool.FileName;
     End;
end;

procedure TForm1.fnKeytoolChange(Sender: TObject);
begin
    unit2.sKeytool:=odKeytool.FileName;
end;

procedure TForm1.spSaveClick(Sender: TObject);
begin
  sdTargetKeystore.InitialDir:=GetUserDir;
  sdTargetKeystore.Title:='Where to save .keystore file?';
  if sdTargetKeystore.Execute then
     begin
       lbKeystore.Caption:='[' + ExtractFileName(sdTargetKeystore.FileName) + '.keystore]';
       lbKeyStore.Hint:=sdTargetKeystore.FileName + '.keystore';
       unit2.sKeystore := sdTargetKeystore.FileName + '.keystore';
     end;

end;

procedure TForm1.spNextClick(Sender: TObject);
var
  fDetail: TForm2;
begin
  if (unit2.sKeystore = '') OR (unit2.sKeytool = '') then
     Begin
       MessageDlg('Keytool and keystore path cannot be empty', mtError, [mbOK], 0);
       Exit;
     End
  else begin
    fDetail := TForm2.Create(self);
    fDetail.ShowModal;
    if fDetail.ModalResult = mrOK then Caption:= Application.Title + ' - COMPLETED!'
    else Caption:= Application.Title + ' - ABORTED!';
  End;
end;

end.

