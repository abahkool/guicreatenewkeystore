unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, StdCtrls;

type

  { TForm2 }

  TForm2 = class(TForm)
    Button1: TButton;
    leAlias: TLabeledEdit;
    leKPass: TLabeledEdit;
    leCN: TLabeledEdit;
    leO: TLabeledEdit;
    leOU: TLabeledEdit;
    leL: TLabeledEdit;
    leST: TLabeledEdit;
    leC: TLabeledEdit;
    leCPass: TLabeledEdit;
    spBack: TSpeedButton;
    spExit: TSpeedButton;
    spKPassShow: TSpeedButton;
    spCPassShow: TSpeedButton;
    spGenerate: TSpeedButton;
    Procedure spExitClick(Sender: TObject);
    procedure spBackClick(Sender: TObject);
    Procedure spCPassShowMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    Procedure spCPassShowMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    Procedure spGenerateClick(Sender: TObject);
    Procedure spKPassShowMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    Procedure spKPassShowMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form2: TForm2;
  sKeytool, sKeystore: string;

implementation

{$R *.lfm}

{ TForm2 }

procedure TForm2.spBackClick(Sender: TObject);
begin
  ModalResult:= mrCancel;
end;

Procedure TForm2.spExitClick(Sender: TObject);
Begin
  Application.Terminate;
end;

Procedure TForm2.spCPassShowMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
Begin
  leCPass.PasswordChar:=#0;
end;

Procedure TForm2.spCPassShowMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
Begin
  leCPass.PasswordChar:='X';
end;

Procedure TForm2.spGenerateClick(Sender: TObject);
var
  dName, sCmd: string;
  slLog: TStringList;

Begin
  //validate input
  if Length(leKPass.Text) < 6 then
  begin
    MessageDlg('Minimum password length = 6', mtError, [mbOK], 0);
    leKPass.SetFocus;
    Exit;
  End;

  if Length(leCPass.Text) < 6 then
  begin
    MessageDlg('Minimum password length = 6', mtError, [mbOK], 0);
    leCPass.SetFocus;
    Exit;
  End;

  if leAlias.Text = '' then
  Begin
    MessageDlg('Alias cannot be empty!', mtError, [mbOK], 0);
    leAlias.SetFocus;
    Exit;
  End;

  //delete existing keystore
  DeleteFile(sKeystore);

  //preparing Distinguished Names
  if leCN.Text <> '' then dName += 'cn=' + leCN.Text + ',';
  if leOU.Text <> '' then dName += 'ou=' + leOU.Text + ',';
  if leO.Text <> '' then dName += 'o=' + leO.Text + ',';
  if leL.Text <> '' then dName += 'l=' + leL.Text + ',';
  if leST.Text <> '' then dName += 'st=' + leST.Text + ',';
  if leC.Text <> '' then dName += 'c=' + leC.Text + ',';

  //Delete last , (comma)
  Delete(dName,LastDelimiter(',',dName),1);

  //preparing keytool to run
  sCmd:= '-genkey -noprompt -keystore "' + sKeystore + '" -alias "'
           + leAlias.Text + '" -keyalg RSA -validity 3650 -storepass "'
           + leKPass.Text + '" -keypass "' + leCPass.Text
           + '" -dname "' + dName + '"';

  ExecuteProcess(sKeytool, sCmd);

  slLog := TStringList.Create;

  slLog.Add('======= CRITICAL INFO  =======');
  slLog.Add('ALIAS: ' + leAlias.Text);
  slLog.Add('KEYSTORE PASSWORD: ' + leKPass.Text);
  slLog.Add('CERTIFICATE / KEY PASSWORD: ' + leCPass.Text);
  slLog.Add('======= CRITICAL INFO  =======');
  slLog.Add('');
  slLog.Add('');
  slLog.Add('======= Command To Run Manually  =======');
  slLog.Add('');
  slLog.Add('"' + sKeytool + '" ' + sCmd);
  slLog.SaveToFile(sKeystore + '.log');
  slLog.Free;

  ModalResult:= mrOK;
end;

Procedure TForm2.spKPassShowMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
Begin
  leKPass.PasswordChar:=#0;
end;

Procedure TForm2.spKPassShowMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
Begin
  leKPass.PasswordChar:='X';
end;

end.

